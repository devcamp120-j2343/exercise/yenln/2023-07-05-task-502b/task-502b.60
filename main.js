import Vehicle from "./vehicle.js";
import Car from "./car.js";
import Motorbike from "./motorbike.js";
var vehicle1 = new Vehicle("Car","Plan");
console.log(vehicle1.print());

console.log(vehicle1 instanceof Vehicle);

var car1 = new Car(2020,"112DTX");
console.log(car1.honk());

console.log(car1 instanceof Car);

var vMotobike1 = new Motorbike(2022,"MM-2XJ");
console.log(vMotobike1.honk());

console.log(vMotobike1 instanceof Motorbike);