class Vehicle {
    constructor(a,b){
        this.ve1 = a;
        this.ve2 = b;
    }
    print(){
        return console.log(this.ve1 + " and " + this.ve2);
    }
}

export default Vehicle;